<?php
namespace perspectiva\phactory;

use Codeception\Util\Stub;
use Yii;
use yii\base\Action;
use yii\console\controllers\MigrateController;
use yii\rbac\DbManager;

class DbCleaner
{
    /**
     * @var \yii\db\Connection
     */
    protected static $db;

    /**
     * @return \yii\db\Connection
     */
    public static function getDb()
    {
        if (self::$db === null) {
            self::$db = Yii::$app->db;
            self::$db->open();
        }

        return self::$db;
    }

    public static function recreate()
    {
        self::clearSchema();
        self::runMigrations();
    }

    public static function clearSchema()
    {
        self::getDb()->pdo->query('BEGIN');
        self::getDb()->pdo->query('DROP SCHEMA public CASCADE');
        self::getDb()->pdo->query('CREATE SCHEMA public');
        self::getDb()->pdo->query('COMMIT');
    }

    protected static function runMigrations()
    {
        if (Yii::$app->authManager instanceof DbManager) {
            self::runMigrationsForDirectory('@yii/rbac/migrations/');
        }

        self::runMigrationsForDirectory('@app/migrations/');

        self::getDb()->schema->refresh();
    }

    protected static function runMigrationsForDirectory($directory = null)
    {
        $migrator = Stub::construct(
            MigrateController::className(),
            ['migrate', 'default'],
            ['stdout' => null]
        );
        $action = new Action('up', $migrator);
        $migrator->interactive = false;
        $migrator->migrationPath = $directory;
        $migrator->beforeAction($action);
        ob_start();
        $migrator->actionUp();
        ob_end_clean();
    }
}
