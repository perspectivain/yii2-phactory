<?php
namespace perspectiva\phactory;

use \FunctionalTester as CodeceptionFunctionalTester;
use Yii;

class FunctionalTester extends CodeceptionFunctionalTester
{
    /**
     * @var array|string configuração da aplicação ou nome do arquivo que a contém
     */
    public $appConfig = '@tests/codeception/config/unit.php';

    /**
     * Antes de cada teste de aceitação
     * 1. Recria o banco de dados de testes (não é possível usar transaction)
     * 2. Instancia um mock da aplicação
     * 3. Configura a Phactory
     */
    public function __construct(\Codeception\Scenario $scenario)
    {
        parent::__construct($scenario);

        DbCleaner::recreate();

        ApplicationMocker::mock($this->appConfig);

        Yii::$app->get('phactory')->init();
    }
}
