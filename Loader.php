<?php

namespace perspectiva\phactory;

class Loader extends \Phactory\Loader
{
    /**
     * @inheritdoc
     */
    public function load($name)
    {
        $factoryClass = 'tests\\factories\\' . ucfirst($name) . 'Phactory';

        if (!class_exists($factoryClass)) {
            throw new \Exception("Unknown factory '$name' ('$factoryClass')");
        }

        return new \Phactory\Factory($name, new $factoryClass);
    }
}
