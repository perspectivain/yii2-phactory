<?php
namespace perspectiva\phactory;

use Phactory;
use yii\base\Component as BaseComponent;

class Component extends BaseComponent
{
    /**
     * Se você possui uma classe triggers, informe o nome completo dela
     * @var string
     */
    public $triggersClass;

    /**
     * Classe responsável por carregar as factories
     * @var string
     */
    public $loaderClass = 'perspectiva\phactory\Loader';

    /**
     * Classe responsável por criar e salvar objetos a partir da factory
     * @var string
     */
    public $builderClass = 'perspectiva\phactory\Builder';

    /**
     * Classe responsável por carregar as dependências da Phactory
     * @var string
     */
    public $dependencyClass;

    /**
     * Classe responsável por carregar os relacionamentos da Phactory
     * @var string
     */
    public $hasOneRelationshipClass;

    /**
     * Inicializa a Phactory configurada de acordo com componente
     */
    public function init()
    {
        parent::init();

        Phactory::reset();
        Phactory::loader(new Loader);
        Phactory::builder(new Builder);

        if ($this->triggersClass) {
            Phactory::triggers(new $this->triggersClass);
        }

        if ($this->dependencyClass) {
            Phactory::$dependencyClass = $this->triggersClass;
        }

        if ($this->hasOneRelationshipClass) {
            Phactory::$hasOneRelationshipClass = $this->hasOneRelationshipClass;
        }
    }
}
