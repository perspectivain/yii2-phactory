<?php

namespace perspectiva\phactory;

use Phactory;
use Yii;

class Test extends \Codeception\TestCase\Test
{
    /**
     * @var array|string configuração da aplicação ou nome do arquivo que a contém
     */
    public $appConfig = '@tests/codeception/config/unit.php';

    /**
     * @var yii\db\Transaction transação atual do banco de dados
     */
    protected $testTransaction;

    /**
     * 1. Mocka uma nova aplicação
     * 2. Configura a Phactory
     * 3. Abre uma transaction do banco relacional
     * 4. Limpa o Redis, se houver
     * 5. Limpa o MongoDB, se houver
     * @inheritdoc
     */
    protected function setUp()
    {
        parent::setUp();

        ApplicationMocker::mock($this->appConfig);

        Yii::$app->get('phactory')->init();

        if (Yii::$app->has('db')) {
            if (false == Yii::$app->db->schema->supportsSavepoint()) {
                throw new \Exception('Your database does not support nested transactions!');
            }

            $this->testTransaction = Yii::$app->db->beginTransaction();
        }

        if (Yii::$app->has('redis')) {
            Yii::$app->redis->executeCommand('FLUSHDB');
        }

        if (Yii::$app->has('mongodb')) {
            $mongodb = Yii::$app->mongodb->getDatabase();
            Yii::$app->mongodb->mongoClient->dropDB($mongodb->name);
        }
    }

    /**
     * 1. Faz o rollback do banco relacional
     * 2. Limpa a aplicação mockada.
     * @inheritdoc
     */
    protected function tearDown()
    {
        // Avoid failing with "Too many connections"
        if (Yii::$app->has('db')) {
            Yii::$app->db->close();
        }

        // If something goes wrong, transaction won't be started
        if ($this->testTransaction) {
            $this->testTransaction->rollBack();
        }

        ApplicationMocker::clean();

        parent::tearDown();
    }
}
