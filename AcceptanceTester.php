<?php
namespace perspectiva\phactory;

use \AcceptanceTester as CodeceptionAcceptanceTester;
use Yii;

class AcceptanceTester extends CodeceptionAcceptanceTester
{
    /**
     * @var array|string configuração da aplicação ou nome do arquivo que a contém
     */
    public $appConfig = '@tests/codeception/config/unit.php';

    /**
     * Antes de cada teste de aceitação
     * 1. Recria o banco de dados de testes (não é possível usar transaction)
     * 2. Instancia um mock da aplicação
     * 3. Configura a Phactory
     */
    public function __construct(\Codeception\Scenario $scenario)
    {
        parent::__construct($scenario);

        DbCleaner::recreate();

        ApplicationMocker::mock($this->appConfig);

        Yii::$app->get('phactory')->init();
    }

    public function fillCKEditorField($id, $value)
    {
        $this->executeJs("CKEDITOR.instances['{$id}'].setData('{$value}');");
    }

    public function markSelect2Option($fieldLabel, $fieldValue)
    {
        $this->executeJs('
        var select2combo = $("label:contains(\''  . $fieldLabel . '\')").parent().find("select");
        window.fieldValue;
        select2combo.find("option").each(function(index, element){
            if ($(element).text().replace(/\W/g, "") == "' . $fieldValue . '".replace(/\W/g, "")) {
                window.fieldValue = $(element).val();
            }
        });
        $("#" + select2combo.attr("id")).select2("val", window.fieldValue);
        ');
    }

    public function removeSelect2Option($fieldLabel, $fieldValue)
    {
        $this->executeJs('
        var select2combo = $("label:contains(\''  . $fieldLabel . '\')").parent().find("select");
        var currentValues = $("#" + select2combo.attr("id")).select2("val");
        window.fieldValue;

        select2combo.find("option").each(function(index, element){
            if ($(element).text().replace(/\W/g, "") == "' . $fieldValue . '".replace(/\W/g, "")) {
                window.fieldValue = $(element).val();
            }
        });

        for (i in currentValues) {
            if (currentValues[i] == window.fieldValue) {
                currentValues.splice(i, 1);
            }
        }

        $("#" + select2combo.attr("id")).select2("val", currentValues);
        ');
    }

    public function clearSelect2($fieldLabel)
    {
        $this->executeJs('
        var select2combo = $("label:contains(\''  . $fieldLabel . '\')").parent().find("select");
        $("#" + select2combo.attr("id")).select2("val", []);
        ');
    }

    public function clickInGrid($searchLabel, $button)
    {
        $this->click("//tr//td[contains(text(), '$searchLabel') or "
            . ".//*[contains(text(), '$searchLabel')]]//..//td//a[@title='$button'"
            . "or text()='$button']");
    }
}
