<?php

namespace perspectiva\phactory;

use Exception;
use yii\db\ActiveRecordInterface;
use Yii;

class Builder extends \Phactory\Builder
{
    /**
     * @param string $name
     * @return string
     */
    protected function getClassName($name)
    {
        return 'app\\models\\' . ucfirst($name);
    }

    protected function toObject($phactoryName, $values)
    {
        $className = $this->getClassName($phactoryName);
        $object = new $className;

        foreach ($values as $key => $value) {
            if ($this->isActiveRecord($object) && $object->getRelation($key, false) !== null) {
                $object->populateRelation($key, $value);
                continue;
            }
            $object->$key = $value;
        }

        if ($this->isActiveRecord($object)) {
            $object->setIsNewRecord(true);
        }

        return $object;
    }

    protected function saveObject($name, $object)
    {
        if ($this->isActiveRecord($object)) {
            foreach ($object->extraFields() as $key) {
                if ($object->isRelationPopulated($key)) {
                    $relatioGetter = 'get' . ucfirst($key);
                    $relation = $object->$relatioGetter();

                    if ($relation) {
                        foreach ($relation->link as $foreignID => $localID) {
                            $object->$localID = $object->$key->$foreignID;
                        }
                    }
                }
            }

            if (false == $object->save()) {
                $message = 'Couldn\'t save the object for "' . $name . '". ';
                $message .= 'Attributes: ' . print_r($object->attributes, true) . '. ';
                $message .= 'Errors: ' . print_r($object->errors, true);
                throw new Exception($message);
            }

        }

        return $object;
    }

    /**
     * Verify if the informed object is an ActiveRecord instance.
     * @param  yii\base\Model  $object object to be verified.
     * @return boolean         wether it is or not an instance of ActiveRecord
     */
    protected function isActiveRecord($object)
    {
        return $object instanceof ActiveRecordInterface;
    }
}
