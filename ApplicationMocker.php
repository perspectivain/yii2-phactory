<?php
namespace perspectiva\phactory;

use Yii;
use yii\base\InvalidConfigException;

/**
 * Helper responsável por criar mocks de uma aplicação a partir de uma configuração.
 */
class ApplicationMocker
{
    /**
     * Cria um mock de aplicação.
     * @param array $config a configuração usada pra criá-la
     * @return \yii\web\Application|\yii\console\Application a instância da aplicação
     * @throws InvalidConfigException se a configuração for inválida
     */
    public static function mock($config = null)
    {
        self::clean();

        if (is_string($config)) {
            $configFile = Yii::getAlias($config);
            if (!is_file($configFile)) {
                throw new InvalidConfigException("O arquivo de configuração não existe: $config");
            }
            $config = require($configFile);
        }

        if (is_array($config)) {
            if (!isset($config['class'])) {
                $config['class'] = 'yii\web\Application';
            }

            return Yii::createObject($config);
        }

        throw new InvalidConfigException('Informe um array de configurações para mockar a aplicação.');
    }

    /**
     * Limpa qualquer aplicação mockada
     */
    public static function clean()
    {
        Yii::$app = null;
    }
}
