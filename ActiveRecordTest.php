<?php

namespace perspectiva\phactory;

use perspectiva\phactory\Test;

class ActiveRecordTest extends Test
{
    /**
     * Make sure all attributes have labels
     */
    public function testAttributesHaveLabels()
    {
        $object = $this->getModelObject();
        $attributes = array_keys($object->attributes);
        $attributesWithLabels = array_keys($object->attributeLabels());
        $attributesWithoutLabel = [];

        foreach ($attributes as $attribute) {
            if (!in_array($attribute, $attributesWithLabels)) {
                $attributesWithoutLabel[] = $attribute;
            }
        }

        if (count($attributesWithoutLabel)) {
            $attributes = implode('", "', $attributesWithoutLabel);
            $message = 'The attributes "' . $attributes . '" do not have labels in attributeLabels';

            $this->fail($message);
        }
    }

    /**
     * Makes sure all attributes have rules
     */
    public function testAttributesHaveRules()
    {
        $object = $this->getModelObject();
        $attributes = $object->attributes;

        foreach ($object->rules() as $rule) {
            $ruleAttributes = (array) $rule[0];

            foreach ($ruleAttributes as $attribute) {
                if ($attribute[0] == '!') {
                    $attribute = substr($attribute, 1);
                }

                if (array_key_exists($attribute, $attributes)) {
                    unset($attributes[$attribute]);
                }
            }
        }

        foreach ($object->scenarios() as $scenarioAttributes) {
            foreach ($scenarioAttributes as $attribute) {
                if ($attribute[0] == '!') {
                    $attribute = substr($attribute, 1);
                }

                if (array_key_exists($attribute, $attributes)) {
                    unset($attributes[$attribute]);
                }
            }
        }

        $primaryKeys = (array) $object->primaryKey();

        foreach ($primaryKeys as $pk) {
            unset($attributes[$pk]);
        }

        if (count($attributes)) {
            $attributes = implode('", "', array_keys($attributes));
            $message = 'The attributes "' . $attributes . '" do not have any validation rules.';

            $this->fail($message);
        }
    }

    /**
     * Adivinha o nome do modelo pelo nome da classe de teste
     * @return string
     */
    protected function getModelClass()
    {
        $className = explode('\\', get_class($this));
        $className = array_pop($className);
        return 'app\\models\\' . substr($className, 0, strlen($className) - strlen('Test'));
    }

    /**
     * Adivinha o nome do modelo pelo nome da classe de teste
     * @return string
     */
    protected function getModelObject()
    {
        $className = $this->getModelClass();
        return new $className;
    }
}
