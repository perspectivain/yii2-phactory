# Integração da Phactory com o Yii2

Esta extensão possiblita integrar os seus testes com a biblioteca
[Phactory](https://github.com/rbone/phactory) ao invés das fixtures que vêm por
padrão.

## Configuração

Adicione as seguintes linhas à seção `require` do seu `composer.json` e rode um
`composer update`:

```json
"perspectiva/yii2-phactory": "~1.0"
```

Após isso, configure um componente `phactory` no seu
`tests\codeception\config\config.php`:

```php
return [
    'components' => [
        'phactory' => 'perspectiva\phactory\Component',
        ...
    ],
    ...
]
```

Caso você queira personalizar a inicialização da Phactory, altere os valores a
seguir na configuração do componente:

```php
'triggersClass' => null,
'loaderClass' => 'perspectiva\phactory\Loader',
'builderClass' => 'perspectiva\phactory\Builder',
'dependencyClass' => 'Phactory\Dependency',
'hasOneRelationshipClass' => 'Phactory\HasOneRelationship',
```

## Gerador de testes para o GII

@TODO

## Utilização

Estenda os seus testes unitários de `perspectiva\phactory\Test`. Se for um teste de
modelo ActiveRecord, estenda de `perspectiva\phactory\ActiveRecordTest`.

Estenda os seus testes de aceitação de `perspectiva\phactory\AcceptanceTester`.

### O que a biblioteca faz?

Se tratando de testes unitários:

1. Mocka uma nova aplicação
2. Configura a Phactory
3. Abre uma transaction do banco relacional (desfeita ao final de cada teste)
4. Limpa o Redis, se existir, antes de cada teste

Se você usar a `ActiveRecordTest`, ele também verificará se:

* Todos os atributos possuem regra de validação?
* Os cenários estão configurados corretamente?

Se tratando de testes de aceitação:

1. Recria o banco de dados de testes (não é possível usar transactions aqui)
2. Mocka uma nova aplicação
3. Configura a Phactory
